<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Currency;

use App\Contracts\CurrencyConversionContract;

class BootstrapAllCurrenciesCommand extends Command
{   
    private $currencyConversionContract;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currencies:bootstrap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Boostrap all currencies from an external API and store to the Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CurrencyConversionContract $currencyConversionContract)
    {
        parent::__construct();
        $this->currencyConversionContract = $currencyConversionContract;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $this->currencyConversionContract->getAllCurrencies();
    }
}

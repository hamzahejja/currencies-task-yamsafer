<?php 

namespace App\Contracts;

interface CurrencyConversionContract {

	public function getAllCurrencies();

	public function getRateForCurrencyByCode($code);
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contracts\CurrencyConversionContract;

class Currency extends Model
{
    
    // protected $fillable = ['name', 'code', 'symbol'];
    protected $guarded = [];

    private $currencyConversionContract;

    public function __construct(){
        $this->currencyConversionContract= app()->make(CurrencyConversionContract::class);
    }


    public function translations()
    {
    	return $this->hasMany(Translation::class, 'currency_id');
    }

    public function addTranslation($translation){
    	$this->translations()->create($translation);
    }

    public function getNameAttribute($value)
    {	
    	//Fetch Request Locale passed in the url as Query String
    	$requestLocale = strtolower(request()->query('locale'));
    	
        /**
    	Perform where clause to fetch transltaion based on the locale
    	Check if translation exists for the currency with the specified Locale
    	Else return the value from the DB table
        **/

    	$currencyTranslation = $this->translations()->where('locale', $requestLocale)->first();

    	if($currencyTranslation != null){
    		return $currencyTranslation->translation_text;
    	} else {
    		return $value;
    	}
    }

    /**
     * TODO:

     * Use rate accessor to get value from the currrency contract rate method
     * store the value in DB
     * if it's in the DB, get it from there
     */

    public function getRateAttribute($value)
    {  
        //Rate already stored in DB
        if($value != 0){
            return $value;
        }

        /**
        Use the Currency Conversion Contract to get rate from External API
        Store/Update the rate in DB
        Return the rate obtained from the Currency Conversion Contract
        **/
        else {

            $conversionRate = $this->currencyConversionContract->getRateForCurrencyByCode($this->code);
            $this->update(['rate' => $conversionRate]);
            return $conversionRate;
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Currency;
use App\Translation;
use Illuminate\Http\Request;
use App\Contracts\CurrencyConversionContract;

class CurrenciesController extends Controller
{
    private $currencyConversionContract;


    public function __construct(CurrencyConversionContract $currencyConversionContract){
        $this->currencyConversionContract = $currencyConversionContract;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        return Currency::paginate(15);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function show($code)
    {   

        // using Conditional Constraint and where to fetch by currency code
         return Currency::where('code', $code)->firstOrFail();

    }


}

   

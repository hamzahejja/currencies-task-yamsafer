<?php 

namespace App\Services;

use App\Currency;
use App\Contracts\CurrencyConversionContract;

class FreeCurrencyConverterService implements CurrencyConversionContract {

    // private $currency;

    // public function __construct(Currency $currency){
    //     $this->currency = $currency;
    // }

    public function getAllCurrencies(){
    
        $currenciesAsString = file_get_contents('https://free.currencyconverterapi.com/api/v6/currencies?apiKey=9fcc862d4f7915d625e2');

        $currenciesAsJSON = json_decode($currenciesAsString);

        $results = $currenciesAsJSON->results;
        foreach ($results as $currencyCode => $currencyData) {

            /**
            Method1: 
            $addedCurrency = new Currency();
            $addedCurrency->name = $currencyData->currencyName;
            $addedCurrency->code = $currencyData->id;

            if(property_exists($currencyData, 'currencySymbol')){
                $addedCurrency->symbol = $currencyData->currencySymbol;
            }

            $addedCurrency->save();
            **/

            /**
            Method2:
            **/
            $currency = Currency::updateOrCreate([
                'name' => $currencyData->currencyName,
                'code' => $currencyData->id,
                'symbol' => property_exists($currencyData, 'currencySymbol') ? $currencyData->currencySymbol: 'none'
            ]);

            //Add Default Translation of Locale 'en' to each Currency created in Database
            //Default Translation Text for English 'en' locale will be the currency name fetched from the external API
            $currency->addTranslation([
                'locale' => 'en',
                'translation_text' => $currency->name
            ]);

        }
    
    }

    public function getRateForCurrencyByCode($code){
        $conversion = 'USD_'. $code;

        $apiUrl = 'https://free.currencyconverterapi.com/api/v6/convert?apiKey=9fcc862d4f7915d625e2&compact=y&q='.$conversion;

        $responseAsString = file_get_contents($apiUrl);

        $responseAsJson = json_decode($responseAsString);

        return $responseAsJson->$conversion->val;


    }




}